package br.edu.iftm.pdm.todoapp.ui.theme

import androidx.compose.ui.graphics.Color

val Purple200 = Color(0xFFBB86FC)
val Purple500 = Color(0xFF6200EE)
val Purple700 = Color(0xFF3700B3)
val Teal200 = Color(0xFF03DAC5)

val White = Color(0xFFFFFFFF)
val Cyan = Color(0xFF00FFFF)
val MossGreen = Color(0xFF3D5C1A)
val AlmostWhite03 = Color(0xFFEFEFEF)
val DarkGray = Color(0xFF585858)
val AlmostBlack01 = Color(0xFF222222)
val AlmostBlack02 = Color(0xFF121212)
val AlmostBlack03 = Color(0xFF252525)
val RedHeart = Color(0xFFF65151)
val TypeRed = Color(0xFFA3170D)