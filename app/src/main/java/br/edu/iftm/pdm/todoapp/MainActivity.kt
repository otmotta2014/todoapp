package br.edu.iftm.pdm.todoapp

import android.os.Build
import android.os.Bundle
import android.view.WindowInsetsController
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.material.MaterialTheme
import androidx.compose.runtime.Composable
import androidx.compose.ui.graphics.toArgb
import br.edu.iftm.pdm.todoapp.data.TaskRequest
import br.edu.iftm.pdm.todoapp.ui.screens.MainScreenView
import br.edu.iftm.pdm.todoapp.ui.theme.TodoAppTheme

class MainActivity : ComponentActivity() {
    private lateinit var taskRequest: TaskRequest
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        this.taskRequest = TaskRequest(this)
        this.taskRequest.startTasksRequest()
        setContent {
            TodoAppTheme {
                this.SetupUIConfigs()
                MainScreenView(this.taskRequest)
            }
        }
    }

    @Composable
    private fun SetupUIConfigs() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.R) {
            if (MaterialTheme.colors.isLight) {
                window.decorView
                    .windowInsetsController?.setSystemBarsAppearance(
                        WindowInsetsController.APPEARANCE_LIGHT_STATUS_BARS,
                        WindowInsetsController.APPEARANCE_LIGHT_STATUS_BARS
                    )
            } else {
                window.decorView
                    .windowInsetsController?.setSystemBarsAppearance(
                        0, WindowInsetsController.APPEARANCE_LIGHT_STATUS_BARS
                    )
            }
        }
        window.statusBarColor = MaterialTheme.colors.primaryVariant.toArgb()
    }
}